
##Amaris Tirtotaroeno  Final Project Video "FollowBot"
<iframe width="880" height="499" src="https://www.youtube.com/embed/eLDLewT2qQA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

##Anchelle Caldeira  Final Project Video "FollowBot"
<iframe width="880" height="499" src="https://www.youtube.com/embed/Hht3HXrUcvI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Amaris Tirtotaroeno & Anchelle Caldeira Presentation Slides "FollowBot"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR2xOysKcPlRyh1EGbmkxgmI57i0cLoI08WyIE1qRb93wruEw8H7k27WALr-G4LmhZGctwM98Etl06P/embed?start=true&loop=true&delayms=3000" frameborder="0" width="880" height="499" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Ghilles Dipotroeno Final Project Video "SailFish"
<iframe width="880" height="499" src="https://www.youtube.com/embed/HQv3MPCPkUY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Axl Redjopawiro and Ishaan Oedit "Forklift "Final Project Video
<iframe width="880" height="499" src="https://www.youtube.com/embed/wq93fKuWJDw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Axl Redjopawiro and Ishaan Oedit Presentation Slide

## Siemla Kuldipsingh "Dispenser Rover"
<iframe width="880" height="499" src="https://www.youtube.com/embed/1sfhudUyrqA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Sherally Dompig "Cleaning Bot"
<iframe width="880" height="499" src="https://www.youtube.com/embed/6Yl1KMa-xTI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Ravina Bansradj "Cleaning Bot"
<iframe width="880" height="499" src="https://www.youtube.com/embed/YOI9eMtQp0U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Keshav Haripersad "Robot Arm Rover"
<iframe width="880" height="499" src="https://www.youtube.com/embed/XAMdfKx7DdU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="880" height="499" src="https://www.youtube.com/embed/4yuIACuCiDM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Varun Brahmadin "Robot Gripper Rover"
<iframe width="880" height="499" src="https://www.youtube.com/embed/tXcHLYNCdoI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="880" height="499" src="https://www.youtube.com/embed/Jc-D_y4JHKs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Track Rover
<iframe width="880" height="499" src="https://www.youtube.com/embed/eMtkm0jJu7I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>